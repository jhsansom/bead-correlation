#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <tuple>
#include <cmath>
#include <limits>
#include <iomanip>
#include <cmath>

#define OMEGA 0.00000005
#define A 0.3
#define B 3
#define MAX_NUM_BEADS 5000
#define NUM_VECTORS 2
#define ACCEPTANCE_STANDARD 0.1

using namespace std;

void print_progress(int i, int max, string task) {
    float progress = (float)i / (float)(max-1);

    int barWidth = 70;

    cout << "Calculating " << task << "\t[";
    int pos = barWidth * progress;
    for (int i = 0; i < barWidth; ++i) {
        if (i < pos) cout << "=";
        else if (i == pos) cout << ">";
        else cout << " ";
    }
    cout << "] " << int(progress * 100.0) << " %\r";
    cout.flush();

    if (i==max-1) {
        cout << endl;
    }
}

void write_prob (float ** prob, int n1, int n2) {
    int i, j;

    ofstream output;
    output.open("prob.csv");

    for (i=0;i<n1;i++) {
        for (j=0;j<n2;j++) {
            output << prob[i][j] << ",";
        }
        output << "\n";
    }

    output.close();
}

tuple<float **, int> read_csv (string filename) {
    ifstream in(filename);
    string line;
    int i=0, j=0, k, rowCount=MAX_NUM_BEADS, colCount=3;
    char ch;

    float ** temp = new float*[rowCount];
    
    i=0;
    while (getline(in, line)) {
        temp[i] = new float[colCount];
        float value;
        stringstream ss(line);

        ss >> temp[i][0];
        ss >> ch;
        ss >> temp[i][1];
        ss >> ch;
        ss >> temp[i][2];

        i++;
    }

    float ** arr = new float*[i];
    
    for (j=0;j<i;j++) {
        arr[j] = new float[colCount];
        for (k=0;k<colCount;k++) {
            arr[j][k] = temp[j][k];
        }
    }

    delete[] temp;

    return make_tuple(arr, i);

}

void write_csv (int ** matches, float * prob, int size, string filename) {
    int i, j;

    ofstream output;
    output.open(filename);

    for (i=0;i<size;i++) {
        for (j=0;j<2;j++) {
            output << matches[i][j] << ",";
        }
        output << prob[i] << ",\n";
    }

    output.close();
}

void print_arr (float ** arr, int n1, int n2) {
    for (int i=0;i<n1;i++) {
        for (int j=0;j<n2;j++) {
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
}

void print_arr2 (int ** arr, int n1, int n2) {
    for (int i=0;i<n1;i++) {
        for (int j=0;j<n2;j++) {
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
}

float c (float * u, float * v) {
    // Calculates angle between vectors
    float dot = 0, mag1 = 0, mag2 = 0;
    for (int i=0;i<3;i++) {
        dot = dot + u[i]*v[i];
        mag1 = mag1 + u[i]*u[i];
        mag2 = mag2 + v[i]*v[i];
    }
    mag1 = sqrt(mag1);
    mag2 = sqrt(mag2);
    float angle = acos(dot/(mag1*mag2));

    if (angle >= 90) {
        return numeric_limits<float>::max();
    } else {
        return abs((mag1/mag2 + mag2/mag1 -2)*((1-cos(angle))/cos(angle)));
    }
}

float Q (float * p, float * q, float * pp, float * qq) {
    float * vp = new float[3];
    float * vq = new float[3];

    for (int i=0;i<3;i++) {
        vp[i] = pp[i] - p[i];
        vq[i] = qq[i] - q[i];
    }

    float cval = c(vp, vq);

    if (cval < OMEGA) {
        return 1.0;
    } else {
        return 0.0;
    }
}

float ** ptilde (float ** p1, float ** p2, int n1, int n2, int ** Cp, int ** Vp, int ** Vq, float ** prev_prob, int size) {
    int i, j, k, l, m, n;
    
    // Calculates equation (5) from the paper based on vectors p and q and Vq, 
    float** prob = new float*[n1];

    // Loops through rows of probability matrix, each one representing a point in p1
    for(i=0;i<n1;i++) {
        print_progress(i, n1, "Ptilde");

        prob[i] = new float[size+1];

        // Loops through points in p2
        for (j=0;j<size;j++) {
            float sum = 0;

            // Sums up points pprime in Vp
            for (k=0;k<size;k++) {
                // Builds vector pprime
                float * pprime = new float[3];
                for (l=0;l<3;l++) {
                    pprime[l] = p1[Vp[i][k]][l];
                }

                // Sums up points qprime in Vq
                for (l=0;l<size;l++) {
                    // Builds vector qprime
                    float * qprime = new float[3];
                    for (m=0;m<3;m++) {
                        qprime[m] = p2[Vq[Cp[i][j]][l]][m];
                        //qprime[m] = p2[Vq[j][l]][m];
                    }

                    float * pp = new float[3];
                    float * qq = new float[3];

                    // Builds pp and qq vectors
                    for (m=0;m<3;m++) {
                        pp[m] = pprime[m] - p1[i][m];
                        qq[m] = qprime[m] - p2[Cp[i][j]][m];
                    }
                    float Qval = Q(p1[i], p2[Cp[i][j]], pp, qq);
                    float numerator = 1 / c(pp, qq);

                    // Calculates denominator for given p->pprime and q->qprimeprime
                    float denominator = 0.0;
                    for (n=0;n<size;n++) {
                        float * qprimeprime = new float[3];
                        for (m=0;m<3;m++) {
                            qprimeprime[m] = p2[Vq[Cp[i][j]][n]][m];
                        }

                        for (m=0;m<3;m++) {
                            pp[m] = pprime[m] - p1[i][m];
                            qq[m] = qprimeprime[m] - p2[Cp[i][j]][m];
                        }

                        denominator = denominator + 1 / c(pp, qq); 
                    }

                    sum = sum + prev_prob[i][j] * numerator / denominator * Qval;
                    
                }

            }

            prob[i][j] = prev_prob[i][j] * (A + B * sum);
        }

        prob[i][size] = prev_prob[i][size];

    }

    return prob;

}


int ** get_closest (float ** p1, float ** p2, int n1, int n2, int size, bool same_idx) {
    // For each row of p1 constituting a 3D vector, get_closest finds the closest 3 vectors in p2
    // and outputs an array with all of them. This can be used to build Cp, Vp, and Vq

    int i, j, k, flag;
    float distance;

    int** idx_arr = new int*[n1];
    for(i=0; i<n1; i++) {
        print_progress(i, n1, "Closest");

        idx_arr[i] = new int[size];

        float dist[size];
        for (k=0;k<size;k++) {
            dist[k] = 10000.0;
        }
        
        for (j=0;j<n2;j++) {

            if (i != j || same_idx) {
                // Distance from current vector in p1 to current vector in p2
                distance = (p1[i][0] - p2[j][0])*(p1[i][0] - p2[j][0]) + (p1[i][1] - p2[j][1])*(p1[i][1] - p2[j][1]) + (p1[i][2] - p2[j][2])*(p1[i][2] - p2[j][2]);
                distance = sqrt(distance);

                float max_dist = 0;
                int idx = -1;
                for (k=0;k<size;k++) {
                    if (dist[k] > max_dist && dist[k] > distance) {
                        max_dist = dist[k];
                        idx = k;
                    }
                }

                if (idx >= 0) {
                    idx_arr[i][idx] = j;
                    dist[idx] = distance;
                }

            }
        }

    }

    float max = 0;
    for (i=0;i<n1;i++) {
        for (j=0;j<size;j++) {
            distance = (p1[i][0] - p2[idx_arr[i][j]][0])*(p1[i][0] - p2[idx_arr[i][j]][0]) + (p1[i][1] - p2[idx_arr[i][j]][1])*(p1[i][1] - p2[idx_arr[i][j]][1]) + (p1[i][2] - p2[idx_arr[i][j]][2])*(p1[i][2] - p2[idx_arr[i][j]][2]);
            distance = sqrt(distance);
            if (distance > max) {
                max = distance;
            }
        }
    }

    cout << "Maximum value : " << max << endl;

    return idx_arr;
}

float *** build_cp (float ** p, int ** idx_arr, int n, int size) {
    // Initializes 3D array, with each array being a new set p2 containing points to map to
    float *** return_arr = new float**[3];
    for (int i=0;i<size;i++) {
        return_arr[i] = new float*[n];
        for (int j=0;j<n;j++) {
            return_arr[i][j] = new float[3];
            for (int k=0;k<3;k++) {
                return_arr[i][j][k] = p[idx_arr[j][i]][k];
            }
        }

    }

    return return_arr;
}

tuple<int **, float *, int> get_pairs (float ** prob, int n1, int n2, int size, int ** Cp) {
    int i, j, k, min;

    if (n1<n2) {
        min = n1;
    } else {
        min = n2;
    }

    int ** matches = new int*[min];
    float * match_prob = new float[min];

    for (i=0;i<min;i++) {
        print_progress(i, min, "Matches");
        matches[i] = new int[2];
        float max_prob = 0;
        int * idx = new int[2];

        // Finds maximum probability in prob matrix
        for (j=0;j<n1;j++) {
            for (k=0;k<size;k++) {
                if (isfinite(prob[j][k])) {
                    if (prob[j][k] > max_prob) {
                        max_prob = prob[j][k];
                        idx[0] = j;
                        idx[1] = Cp[j][k];
                    }
                } else {
                    prob[j][k] = 0.0;
                }
            }
        }

        // Uses this maximum probability to add to matches
        if (max_prob > ACCEPTANCE_STANDARD) {
            matches[i][0] = idx[0];
            matches[i][1] = idx[1];
            match_prob[i] = max_prob;
        } else {
            int ** matches2 = new int*[i];
            float * match_prob2 = new float[min];
            for (j=0;j<i;j++) {
                match_prob2[j] = match_prob[j];
                matches2[j] = new int[2];
                for (k=0;k<2;k++) {
                    matches2[j][k] = matches[j][k];
                }
            }
            print_progress(min, min, "Matches");
            cout << endl;
            return make_tuple(matches2, match_prob2, i);
        }

        for (j=0;j<size;j++) {
            prob[idx[0]][j] = 0.0;
        }

    }

    return make_tuple(matches, match_prob, min);

}

float ** get_prob (float ** p1, float ** p2, int n1, int n2, int size) {
    int i, j, k;

    // Initializes probability array with p along row and q along column. Last column (index=n2) is P*
    float init_prob = 1.0 / ((float)n1 + 1.0);
    float** prob = new float*[n1];
    for(i=0; i<n1; ++i) {
        prob[i] = new float[size+1];
        for (j=0;j<size+1;j++) {
            prob[i][j] = init_prob;
        }
    }

    cout << "Initialized arrays" << endl;

    // Create array of Cp
    int ** Cp = get_closest(p1, p2, n1, n2, size, false);

    // Create array of Vp
    int ** Vp = get_closest(p1, p1, n1, n1, size, false);

    // Create array of Vq
    int ** Vq = get_closest(p2, p2, n2, n2, size, false);

    // Calculate ptilde
    float ** pt = ptilde(p1, p2, n1, n2, Cp, Vp, Vq, prob, size);

    //write_prob(prob, n1, size+1);

    for (i=0;i<n1;i++) {
        float sum = 0;
        for (j=0;j<size;j++) {
            sum += pt[i][j];
        }
        for (j=0;j<size;j++) {
            pt[i][j] = pt[i][j] / (sum + pt[i][size]);
        }
        pt[i][size] = pt[i][size] / (sum + pt[i][size]);
    }

    write_prob(pt, n1, size+1);

    // Get matches
    auto pairs = get_pairs(pt, n1, n2, size, Cp);
    int ** matches = get<0>(pairs);
    float * probs = get<1>(pairs);
    int min = get<2>(pairs);

    write_csv(matches, probs, min, "matches.csv");

    return pt;
}
  

int main () {

    // Get data from first csv file
    auto data1 = read_csv("initial_xyz.txt");
    float ** p1 = get<0>(data1);
    int n1 = get<1>(data1);
    

    // Get data from second csv file
    auto data2 = read_csv("final_xyz_smalldisp.txt");
    float ** p2 = get<0>(data2);
    int n2 = get<1>(data2);

    /*
    int n2 = n1;
    float ** p2 = p1;
    for (int i=0;i<n1;i++) {
        for (int j=0;j<3;j++) {
            p2[i][j] = p1[i][j] + 1.0;
        }
    }
    */

    float ** prob = get_prob(p1, p2, n1, n2, NUM_VECTORS);
    int ** Cp = get_closest(p1, p2, n1, n2, 3, false);

    int num = 0;
    for (int i=0;i<n1;i++) {
        float max_prob = 0;
        int idx = 0;
        for (int j=0;j<3;j++) {
            if (prob[i][j] > max_prob) {
                max_prob = prob[i][j];
                idx = j;
            }
        }
    }

}


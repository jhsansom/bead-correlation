import numpy as np 
import pandas as pd 
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

program = np.array(pd.read_csv('./matches.csv', header=None))
true = np.array(pd.read_csv('./final.csv', header=None))

program = program[:,0:3]


m1 = int(max(program[:,0]))+1
arr1 = np.zeros(m1) - 1
arr2 = np.zeros(m1) - 1

for i in range(len(program)):
    idx = int(program[i,0])
    arr1[idx] = int(program[i,1])

    idx = int(true[i,0])
    arr2[idx] = int(true[i,1])

num = 0
vec1 = np.array(pd.read_csv('./initial_xyz.txt', header=None))
vec2 = np.array(pd.read_csv('./final_xyz_smalldisp.txt', header=None))

graph_vec = np.zeros(6)
for i in range(len(arr1)):
    if arr1[i] == arr2[i]:# or arr1[i] == -1:
        num += 1

for i in range(len(program)-1):
    #print(program[i,0])
    #print(program[i,1])
    vec = np.append(vec1[int(program[i,0]),:], vec2[int(program[i,1]),:] - vec1[int(program[i,0]),:])
    if arr1[i] == arr2[i]:
        graph_vec = np.vstack((graph_vec, vec))

print(graph_vec)
X, Y, Z, U, V, W = zip(*graph_vec)
fig = plt.figure()
ax = Axes3D(fig)
ax.quiver(X, Y, Z, U, V, W)
ax.set_xlim([0, np.amax(vec2[:,0])])
ax.set_ylim([0, np.amax(vec2[:,1])])
ax.set_zlim([0, np.amax(vec2[:,2])])
plt.show()

perc = (num/program.shape[0]) * 100

print('Accuracy = %.2f %%'%perc)

